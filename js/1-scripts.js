var accordion = new BadgerAccordion('.js-badger-accordion');

var button = document.querySelector('#toggleMode');
var Switch = document.querySelector('#switch');
var body = document.body;
var logo = document.querySelector('#logo');
var lamp = document.querySelector('#lamp');



if(localStorage.getItem('mode') == 'dark') {
    body.classList.add('dark');
    Switch.checked = true;
    logo.src = '/img/primr-logo-dark.svg';
    lamp.src = '/img/lamp-off.svg';
} else {
    body.classList.remove('dark');
}

Switch.onchange = function () {
    body.classList.toggle('dark');
    if (body.classList.contains('dark')) {
        logo.src = '/img/primr-logo-dark.svg';
        lamp.src = '/img/lamp-off.svg';
    } else {
        logo.src = '/img/primr-logo-light.svg';
        lamp.src = '/img/lamp-on.svg';
    }
    if (localStorage.getItem('mode') == 'dark') {
        localStorage.setItem('mode', 'light');
    } else {
        localStorage.setItem('mode', 'dark');
    }
    console.log(localStorage.getItem('mode'))
}

console.log(localStorage.getItem('mode'))

// button.onclick = function() {
//     body.classList.toggle('dark')
// }
// for (var i = 0; i < button.length; i++) {
//     button[i].onclick = function () {
//         if(button.getAttribute('aria-expanded' === false)) {
//             body.classList.toggle('dark');
//         } else {
//             body.classList.toggle('dark');
//         }
//     };
// }
